<!DOCTYPE>
<html>
 <head>
     <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
     <link rel="stylesheet" type="text/css"  href="View/style/style.css" />

     <meta charset="utf-8" />
     <title>S'inscrire</title>
 </head>
    <body>

    <?php include("View/include/menu.php"); ?>
    <section>
            <h1 class="actuH1">Inscription</h1>
        <hr/>
            <form data-toggle="validator" role="form" method="POST" ACTION="index.php?home=signup">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputPrenom">Prénom</label>
                        <input name="prenom" class="form-control" id="inputPrenom" placeholder="prénom"  data-error= "Minimum 3 caractères" minlength="3" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputNom">nom</label>
                        <input name="nom" class="form-control" id="inputNom" placeholder="nom" data-error="Minimum 3 caractères" minlength="3" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail" class="control-label">Email</label>
                    <input name="email" type="email" class="form-control" id="inputEmail" aria-describedby="emailHelp" placeholder="Email" required>
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                <div class="form-group">
                    <label for="inputUsername" class="control-label">Username</label>
                    <input id="inputUsername" name="username" minlength="3" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="inputPassword">password</label>
                    <input id="inputPassword" name="password"  type="password" class="form-control" required>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>

            </form>




    </section>
    <?php include("View/include/footer.php")?>
    </body>
</html>