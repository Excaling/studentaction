<header class="navbar navbar-dark viasup-color"><a href="index.php"><h1>StudentAction</h1></a>
<nav class="navbar navbar-expand-lg navbar-dark viasup-color" >
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Qui sommes nous?</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="index.php?home=video">Vidéo</a>
                            <a class="dropdown-item" href="index.php?home=vision">Notre vision</a>
                            <a class="dropdown-item" href="index.php?home=mission">Nos mission</a>
                            <a class="dropdown-item" href="index.php?home=ambition">Notre ambition</a>
                            <a class="dropdown-item" href="index.php?home=presentation">Présentation de l’équipe : 3 colonnes (Bureau/ CA/ Noms des Pôles) </a>
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Nos actions</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="index.php?home=consultTutorat">Tutorat (via AFEV + anciens) </a>
                            <a class="dropdown-item" href="index.php?home=consultEvent">Organisation d’événements </a>
                            <a class="dropdown-item" href="index.php?home=consult">Promouvoir la CPES</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="index.php?home=actu">Actualité</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="index.php?home=soutient">Soutenez nous</a>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Engagez-vous</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="index.php?home=engagePro">Pour les professionnels qui veulent tuteurer des CPES  </a>
                            <a class="dropdown-item" href="index.php?home=engageAncien">Pour des anciens qui veulent devenir tuteurs  </a>
                            <a class="dropdown-item" href="index.php?home=tutorat">Tutorat via l’AFEV</a>
                        </div>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="index.php?home=nosPartenaires">Nos partenaires</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="index.php?home=temoignage">Témoignage</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Annuaire</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Calendrier</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Forum</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Contact</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="index.php?home=signup">S'inscrire</a>
                    </li>
                </ul>
            </div>
            </nav>
</header>