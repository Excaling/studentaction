<?php
    include_once("Job/Groupe.php");
    include_once("connect.inc.php");
    class ModeleGroupe{
        public function getListeGroupes(){
            global $bdd;
            $req = $bdd->prepare("SELECT * FROM Groupes");
            $res = $req->execute();
            foreach($res as $grp){
                $ListeGroupes[] = new Groupe($grp['idGroupe'], $grp['nomGroupe']);
            }
            return $ListeGroupes;
        }
    }

?>