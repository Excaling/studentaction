
<?php 
    include_once("Modele/ModeleUtilisateur.php");
    class ControllerUtilisateur{
        private $modeleUtilisateur;

        public function __construct(){
            $this->modeleUtilisateur = new ModeleUtilisateur();
        }

        public function getListeUtilisateurs(){
            $ListeUtilisateurs[] = $this->modeleUtilisateur->getListeUtilisateurs();
        }

        public function getUtilisateur($idUser){
            $Utilisateur = $this->modeleUtilisateur->getUtilisateur($idUser);
        }

    }

?>